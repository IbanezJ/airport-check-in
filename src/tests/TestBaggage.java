package tests;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import  org.junit.Assert.*;
import org.junit.Assert;
import main.Baggage;
import main.Flight;
import main.Kiosk;

public class TestBaggage {
	
	@Test
	public void TestBaggagePrice() {
		int [] size = {34,56,24};
		Baggage myBaggage = new Baggage(19.00,size);
		float calculatePrice = myBaggage.calculatePrice();
		int expected = 0;
		assertEquals("price is equal?",expected,calculatePrice, 0.001);
		
		int [] size2 = {100,30,40};
		Baggage myBaggage2 = new Baggage(35,size2);
		float calculatePrice2 = myBaggage2.calculatePrice();
		int expected2 = 75;
		assertEquals("price is equal?",expected2,calculatePrice2, 0.001);
		
        System.out.println("Hello Airport Check-In!");
        Baggage test = new Baggage();
        int [] size3 = {1,3,4};
        test.setDimension(size3);
        System.out.println((Arrays.toString(test.getDimension())));
        int [] size4 = {1,3,4,5};
        test.setDimension(size4);
        System.out.println((Arrays.toString(test.getDimension())));
	}
	
	
	
}

